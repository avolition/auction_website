import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import './index.css';
import App from './components/App';
// import registerServiceWorker from './registerServiceWorker';

const alertOptions = {
  transition: 'fade',
  position: 'top left',
  offset: '25% 10%',
  timeout: 5000
};

ReactDOM.render(
  <BrowserRouter>
    <AlertProvider template={AlertTemplate} {...alertOptions}>
      <Route path="/" component={App} />
    </AlertProvider>
  </BrowserRouter>,
  document.getElementById('root')
);
// registerServiceWorker();
