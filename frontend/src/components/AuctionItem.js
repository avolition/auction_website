import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import AuctionItemCard from './AuctionItemCard';
import AuctionItemBidForm from './AuctionItemBidForm';

class AuctionItem extends Component {
  render() {
    const item = this.props.auctionItems.find(el => el._id === this.props.match.params.id);
    return [
      <section key="notNeeded1" className="row">
        <div className="hide-for-small-only medium-1 large-2 columns" />
        <div className="medium-10 large-8 columns">
          <Link to="/auctions">
            <button className="expanded button">View All Auctions</button>
          </Link>
          {item ? <AuctionItemCard item={item} i={'item0'} /> : null}
        </div>
        <div className="hide-for-small-only medium-1 large-2 columns" />
      </section>,
      item ? (
        <AuctionItemBidForm updateItem={this.props.updateItem} key="notNeeded2" item={item} />
      ) : null
    ];
  }
}

export default withRouter(AuctionItem);
