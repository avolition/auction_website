import React from 'react';

const Navbar = props => (
  <header className="title-bar">
    <h4 className="title-bar-left title-bar-title">{props.title}</h4>
  </header>
);

export default Navbar;
