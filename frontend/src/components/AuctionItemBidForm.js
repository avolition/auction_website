import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { withAlert } from 'react-alert';
import axios from 'axios';
import moment from 'moment';
import { users } from '../js/userdata';
import { apiURL } from '../js/helpers';

class AuctionItemBidForm extends Component {
  state = {
    bid: undefined
  };
  // 2018-05-17 14:55:00.000
  userRef = React.createRef();
  bidRef = React.createRef();
  submitItem = async e => {
    e.preventDefault();
    const newBid = {
      itemId: this.props.match.params.id,
      bid: this.state.bid * 100 // convert dollars to cents
    };
    const headers = { authorization: `Bearer ${users[this.userRef.current.value].bearer}` };
    const request = axios.create({
      headers,
      url: 'api/auctions/item/bid',
      baseURL: apiURL,
      method: 'post',
      data: { newBid }
    });
    const result = await request();
    const { data } = result;
    if (result.data && result.data.success) {
      if (data.success && data.success.msg) this.props.alert.success(data.success.msg);
      this.props.updateItem(data.item);
    } else if (result.data && result.data.errors) {
      if (data.errors && data.errors.msg) this.props.alert.error(data.errors.msg);
    }
  };
  bidTextGenerator = () => {
    const bid = Number(this.bidRef.current.value).toFixed(2);
    this.setState({ bid });
  };
  formReset = e => {
    e.preventDefault();
    document.querySelector('form').reset();
  };
  render() {
    const { item } = this.props;
    const over = moment(item.endDate).format() < moment(Date.now()).format();
    const usernames = ['username1', 'username2', 'username3'];
    return (
      <section className="row">
        <div className="hide-for-small-only medium-1 large-3 columns" />
        <form
          className="medium-10 large-6 columns"
          action="/api/auctions/item/bid"
          method="POST"
          onSubmit={this.submitItem}
        >
          <label>
            Place bid as user
            <select
              defaultValue="select hardcoded user"
              name="user"
              ref={this.userRef}
              disabled={over}
              required
            >
              <option disabled>select hardcoded user</option>
              {usernames.map((name, i) => {
                const thisUser = item.owner.username === name;
                return (
                  <option key={i} disabled={thisUser}>
                    {name} {thisUser ? '(owner)' : null}
                  </option>
                );
              })}
            </select>
          </label>
          <label>
            Enter bid (minimum bid for this item: $ {(item.minBid / 100).toFixed(2)})
            <input
              type="number"
              name="bid"
              placeholder="enter bid"
              ref={this.bidRef}
              step={0.01}
              aria-describedby="currencyDisplay"
              onChange={this.bidTextGenerator}
              disabled={over}
              required
            />
            <p className="help-text" id="currencyDisplay">
              Your bid: ${' '}
              {this.state.bid
                ? this.state.bid.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
                : 0.01}
            </p>
          </label>
          <input className="button success" type="submit" value="Submit Your Bid" />
          <button onClick={this.formReset} className="button alert float-right">
            Cancel
          </button>
        </form>
        <div className="hide-for-small-only medium-1 large-3 columns" />
      </section>
    );
  }
}

export default withRouter(withAlert(AuctionItemBidForm));
