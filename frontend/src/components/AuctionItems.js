import React from 'react';
import { Link } from 'react-router-dom';
import AuctionItemCard from './AuctionItemCard';

const AuctionItems = props => (
  <section className="row">
    <div className="hide-for-small-only medium-1 large-2 columns" />
    <div className="medium-10 large-8 columns">
      <Link to="/auctions/new">
        <button className="expanded button">Create an Auction</button>
      </Link>
      {props.auctionItems ? (
        props.auctionItems.map((item, i) => {
          return <AuctionItemCard key={i} i={i} item={item} />;
        })
      ) : (
        <p>No auctions to display</p>
      )}
    </div>
    <div className="hide-for-small-only medium-1 large-2 columns" />
  </section>
);

export default AuctionItems;
