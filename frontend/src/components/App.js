import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { withAlert } from 'react-alert';
import Navbar from './Navbar';
// import Aside from './Aside';
import AuctionItems from './AuctionItems';
import AuctionItemForm from './AuctionItemForm';
import AuctionItem from './AuctionItem';
import axios from 'axios';
import { apiURL } from '../js/helpers';

class App extends Component {
  state = {
    auctionItems: []
  };
  // temp solution for ordering auction items by earliest date/time
  orderAuctions = allAuctions => {
    return allAuctions.sort((a, b) => (a.endDate < b.endDate ? -1 : 1));
  };
  refreshState = async () => {
    const { data } = await axios.get(`${apiURL}/api/auctions`);
    const auctionItems = this.orderAuctions(data.items);
    this.setState({ auctionItems });
  };
  addNewItemToLocalState = newItem => {
    let allAuctions = this.state.auctionItems;
    allAuctions.push(newItem);
    const auctionItems = this.orderAuctions(allAuctions);
    this.setState({ auctionItems });
  };
  updateItem = updatedItem => {
    const allAuctions = this.state.auctionItems;
    const auctionItems = allAuctions.map(
      item => (item._id === updatedItem._id ? updatedItem : item)
    );
    this.setState({ auctionItems });
  };
  componentWillMount = async () => {
    this.refreshState();
    this.refreshStateInterval = setInterval(this.refreshState, 1000);
  };
  componentWillUnmount = () => {
    clearInterval(this.refreshStateInterval);
  };
  render() {
    const { auctionItems } = this.state;
    return (
      <main>
        <Navbar title="Auctionator" />
        <Redirect from="/" to="/auctions" />
        <Switch>
          <Route
            exact
            path="/auctions"
            render={() => <AuctionItems auctionItems={auctionItems} />}
          />
          <Route
            path="/auctions/new"
            render={() => <AuctionItemForm addNewItemToLocalState={this.addNewItemToLocalState} />}
          />
          <Route
            path="/auctions/:id"
            render={() => <AuctionItem updateItem={this.updateItem} auctionItems={auctionItems} />}
          />
        </Switch>
      </main>
    );
  }
}

export default withAlert(App);
