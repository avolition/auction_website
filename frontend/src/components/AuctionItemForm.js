import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { withAlert } from 'react-alert';
import axios from 'axios';
import moment from 'moment';
import { users } from '../js/userdata';
import { apiURL } from '../js/helpers';

class AuctionItemForm extends Component {
  state = {
    minBid: undefined
  };
  userRef = React.createRef();
  titleRef = React.createRef();
  descRef = React.createRef();
  imageRef = React.createRef();
  minBidRef = React.createRef();
  endDateRef = React.createRef();

  submitItem = async e => {
    e.preventDefault();
    const newItem = {
      title: this.titleRef.current.value,
      desc: this.descRef.current.value,
      image: this.imageRef.current.value,
      minBid: this.state.minBid * 100, // convert dollars to cents
      endDate: moment.utc(this.endDateRef.current.value).format()
    };
    const headers = { authorization: `Bearer ${users[this.userRef.current.value].bearer}` };
    const request = axios.create({
      headers,
      url: 'api/auctions/new',
      baseURL: apiURL,
      method: 'post',
      data: { newItem }
    });
    const result = await request();
    const { data } = result;
    if (result.status === 200) {
      this.props.addNewItemToLocalState(data.item);
      this.props.history.push(`/auctions/${data.item._id}`);
      if (data.success && data.success.msg) this.props.alert.success(data.success.msg);
    } else {
      data.errors.forEach(err => this.props.alert.error(err.msg));
    }
  };
  bidTextGenerator = () => {
    const minBid = Number(this.minBidRef.current.value).toFixed(2);
    this.setState({ minBid });
  };
  render() {
    return (
      <section className="row">
        <div className="hide-for-small-only medium-1 large-3 columns" />
        <form
          className="medium-10 large-6 columns"
          action="/api/auctions/new"
          method="POST"
          onSubmit={this.submitItem}
        >
          <h4>Auction Item</h4>
          <label>
            Submit item as user
            <select defaultValue="select hardcoded user" name="user" ref={this.userRef} required>
              <option disabled>select hardcoded user</option>
              <option>username1</option>
              <option>username2</option>
              <option>username3</option>
            </select>
          </label>
          <label>
            Title
            <input
              type="text"
              name="title"
              maxLength={150}
              placeholder="item title"
              ref={this.titleRef}
              required
            />
          </label>
          <label>
            Description
            <textarea
              name="desc"
              placeholder="item description"
              maxLength={500}
              ref={this.descRef}
              required
            />
          </label>
          <label>
            Image URL
            <input type="url" name="image" placeholder="image url" ref={this.imageRef} disabled />
          </label>
          <label>
            Starting bid
            <input
              type="number"
              name="minBid"
              placeholder="minimum bid"
              ref={this.minBidRef}
              aria-describedby="currencyDisplay"
              onChange={this.bidTextGenerator}
              required
            />
            <p className="help-text" id="currencyDisplay">
              Minimum starting bid: ${' '}
              {this.state.minBid
                ? this.state.minBid.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
                : 0.01}
            </p>
          </label>
          <label>
            Auction end date / time
            <input
              type="datetime-local"
              name="date"
              placeholder="date"
              ref={this.endDateRef}
              min={moment()
                .add(1, 'minute')
                .format('YYYY-MM-DDTHH:mm')}
              required
            />
          </label>
          <input className="button success" type="submit" value="Create Auction" />
          <Link to="/auctions" className="button alert float-right">
            Cancel
          </Link>
        </form>
        <div className="hide-for-small-only medium-1 large-3 columns" />
      </section>
    );
  }
}

export default withRouter(withAlert(AuctionItemForm));
