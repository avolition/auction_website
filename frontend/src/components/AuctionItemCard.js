import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import image from '../assets/images/noimage.png';
import moment from 'moment';

class AuctionItemCard extends Component {
  state = {
    auctionEnd: undefined
  };
  // 2018-05-17 13:45:00.000
  // fn to refresh display timer for item based on endDate
  timerRefresh = () => {
    const { endDate } = this.props.item;
    const endDateMS = moment(endDate).valueOf();
    let auctionEnd = moment(endDate).fromNow();
    if (moment(Date.now()).valueOf() > moment(endDateMS).valueOf()) {
      auctionEnd = 'Auction Over';
      this.setState({ auctionEnd });
      document.getElementById('timeDisplay').classList.add('red');
      return clearInterval(this.timerCountdown);
    } else if (auctionEnd === 'in a minute' || auctionEnd === 'in a few seconds') {
      auctionEnd = `in ${Math.round((endDateMS - moment(Date.now()).valueOf()) / 1000)} seconds`; // seconds countdown
    }
    if (auctionEnd === this.state.auctionEnd) return;
    this.setState({ auctionEnd });
  };
  componentWillMount = () => {
    this.timerCountdown = setInterval(this.timerRefresh, 1000);
  };
  componentWillUnmount = () => {
    clearInterval(this.timerCountdown);
  };
  render() {
    const { item, i, match } = this.props;
    const { auctionEnd } = this.state;
    return (
      <article key={i} className="media-object">
        <div className="media-object-section hide-for-small-only">
          <img width="150" src={image} alt={item.title} />
        </div>
        <div className="media-object-section">
          <h4>{item.title}</h4>
          <p>{item.desc}</p>
          <p className="media-object-details">
            <span className="bold">Auction ending: </span>
            <time
              id="timeDisplay"
              className={
                auctionEnd && (auctionEnd.includes('seconds') || auctionEnd === 'Auction Over')
                  ? 'red'
                  : ''
              }
            >
              {auctionEnd}
            </time>
          </p>
          <p className="media-object-details">
            <span className="bold">Winning bid: </span>
            <span className={auctionEnd === 'Auction Over' ? 'red bold' : ''}>
              {(item.highestBid &&
                ' $ ' +
                  Number(item.highestBid / 100)
                    .toFixed(2)
                    .toString()
                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')) ||
                (!item.highestBid && auctionEnd === 'Auction Over' && 'Item not sold') ||
                (auctionEnd && 'No bids yet. Place a bid!')}
              {item.highestBid &&
                auctionEnd === 'Auction Over' &&
                ` - by ${item.highestBidder.username}`}
            </span>
            {!match.params.id ? (
              <Link to={`/auctions/${item._id}`} className="button label float-right">
                View Auction Item
              </Link>
            ) : null}
          </p>
        </div>
      </article>
    );
  }
}

export default withRouter(AuctionItemCard);
