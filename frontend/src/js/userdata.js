export const users = {
  username1: {
    bearer:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXJuYW1lMSIsImVtYWlsIjoiZW1haWwxQHRlc3QuY29tIiwiZGF0ZSI6MTUyNjAxMjE2NTEyMSwiaWF0IjoxNTI2MDEyMTY1LCJleHAiOjE1NTc1Njk3NjV9.GQTiRDR_d0yityTYmMQwmS8aLKlnUpUYusPiMRRJMHQ'
  },
  username2: {
    bearer:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXJuYW1lMiIsImVtYWlsIjoiZW1haWwyQHRlc3QuY29tIiwiZGF0ZSI6MTUyNjAxMzA1MzQ1OSwiaWF0IjoxNTI2MDEzMDUzLCJleHAiOjE1NTc1NzA2NTN9.SNb7gVfr8V6aDS499JLB3-pBylrCaPP2BGEYoNdah2M'
  },
  username3: {
    bearer:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXJuYW1lMyIsImVtYWlsIjoiZW1haWwzQHRlc3QuY29tIiwiZGF0ZSI6MTUyNjAxMzA5NDc1MCwiaWF0IjoxNTI2MDEzMDk0LCJleHAiOjE1NTc1NzA2OTR9.ZtLOLIhzJmhdYxhHj5qX_JiPwgtgoDimW-WOKM0jyWM'
  }
};
