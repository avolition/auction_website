# CODING CHALLENGE - Auction website

## The challenge

As a developer at Edapp you will be responsible for the implementation of new features in the existing technology stack. That said, not everything in our platform is set in stone yet and you will have a lot of influence on the future of our platform. Application architecture is the shared responsibility of all the members in the development team and we expect input from everyone on this. Often, we work with very high-level requirements which requires a pro-active attitude, good communication skills and a creative mind.

To demonstrate your capability, we wish to set the following challenge:

**Design and build a simple auction website, like Ebay, where people can see a list of items and the ability to bid on them. The auction must have an end date and time, after which the item is sold to the highest bidder. The goal of this exercise is to demonstrate that you can take an idea with very little requirements and turn that into a working prototype. The missing requirements will need creativity and common sense to solve.**

**Getting started:**

* Pick a language from .Net Core or NodeJs for the backend
* Choose a front-end frame work that you like working with
* Think about maintainability of the solution by other developers
* We would like to see some automated tests on this solution
* The challenge is about the auction functionality. Therefore items, users, etc can be hard coded.

**Build and demonstration:**

* Be prepared to give a 15 minute demo of your solution and explain your choices.
* 30 minutes for interactive questions
* Please send us the code through a link to a github or bitbucket repository.

---

---

# Implementation

### Hardcoded Users:

When a user is selected from the dropdown on the front-end, the bearer token is set and verified against the db, to mock JWT authentication. Users created via Postman POST requests to '/api/auth/register'

{
"\_id": "5af519052c0ae56a5417e3d5",
"email": "email1@test.com",
"password": "$2a$10$NgTe3.jaMPw515PCOSOqouMOUmdc.vBYItLhw0tJ9ERjyjvTpn8aW",
"username": "username1",
"created": "2018-05-11T04:16:05.018Z",
"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXJuYW1lMSIsImVtYWlsIjoiZW1haWwxQHRlc3QuY29tIiwiZGF0ZSI6MTUyNjAxMjE2NTEyMSwiaWF0IjoxNTI2MDEyMTY1LCJleHAiOjE1NTc1Njk3NjV9.GQTiRDR_d0yityTYmMQwmS8aLKlnUpUYusPiMRRJMHQ",
"\_\_v": 0
}

{
"\_id": "5af51c7d2c0ae56a5417e3d6",
"email": "email2@test.com",
"password": "$2a$10$.LQlzkBKgRp2mYn6kctCXuN9pV4MGNzl3Evo/S.cbWUosc6VPqHWW",
"username": "username2",
"created": "2018-05-11T04:30:53.368Z",
"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXJuYW1lMiIsImVtYWlsIjoiZW1haWwyQHRlc3QuY29tIiwiZGF0ZSI6MTUyNjAxMzA1MzQ1OSwiaWF0IjoxNTI2MDEzMDUzLCJleHAiOjE1NTc1NzA2NTN9.SNb7gVfr8V6aDS499JLB3-pBylrCaPP2BGEYoNdah2M",
"\_\_v": 0
}

{
"\_id": "5af51ca62c0ae56a5417e3d7",
"email": "email3@test.com",
"password": "$2a$10$NjZ30/5cIkI4J9LPmJLjWefVieizYJg1bRrMRjRK.qisxOAhcTvzm",
"username": "username3",
"created": "2018-05-11T04:31:34.663Z",
"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXJuYW1lMyIsImVtYWlsIjoiZW1haWwzQHRlc3QuY29tIiwiZGF0ZSI6MTUyNjAxMzA5NDc1MCwiaWF0IjoxNTI2MDEzMDk0LCJleHAiOjE1NTc1NzA2OTR9.ZtLOLIhzJmhdYxhHj5qX_JiPwgtgoDimW-WOKM0jyWM",
"\_\_v": 0
}
