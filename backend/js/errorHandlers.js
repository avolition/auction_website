// async/await higher order function Catch Errors Handler
exports.catchErrors = fn => {
  return function(req, res, next) {
    return fn(req, res, next).catch(next);
  };
};

// MongoDB Validation Error Handler
// Detect if there are mongodb validation errors to show via flash messages
exports.flashValidationErrors = (err, req, res, next) => {
  console.error('***** ERROR: flashValidationErrors triggered: ', err);
  // if no errors that can be flashed, skip to next function
  if (!err.errors) return next(err);
  // validation errors look like
  return res.status(200).json({ errors: err.errors });
};

// Development Error Handler
exports.developmentErrors = (err, req, res, next) => {
  console.error('***** ERROR: developmentErrors triggered: ', err);
  err.stack = err.stack || '';
  const errorDetails = {
    message: err.message,
    status: err.status,
    stackHighlighted: err.stack.replace(/[a-z_-\d]+.js:\d+:\d+/gi, '<mark>$&</mark>')
  };
  res.status(err.status || 500).json(errorDetails);
};

// Production Error Handler - No stacktraces are leaked to user
exports.productionErrors = (err, req, res, next) => {
  console.error('***** ERROR: productionErrors triggered');
  const error = {
    message: err.message,
    error: {}
  };
  res.status(err.status || 500).json(error);
};
