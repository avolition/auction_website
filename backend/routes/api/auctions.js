const moment = require('moment');

// import models
const Item = require('../../models/Item');
const User = require('../../models/User');

// @route GET api/auctions
// @desc query a specific location in database
// @access Public
exports.showItems = async (req, res) => {
  const allItems = await Item.find();
  res.json({ items: allItems });
};

// @route POST api/auctions/item
// @desc view auction item
// @access Public
exports.showItem = (req, res) => {
  res.json({ type: 'POST api/auctions/item' });
};

// @route POST api/auctions/new
// @desc add auction item
// @access Private
exports.addItem = async (req, res) => {
  const { newItem } = req.body;
  if (!newItem.image) newItem.image = 'noimage.png';
  newItem.owner = req.body.user;
  // TODO: validate / sanitize data
  const item = await new Item(newItem).save();
  res.json({
    item,
    success: {
      msg: `New auction item created successfully!`
    }
  });
};

// // @route POST api/auctions/item/edit
// // @desc add auction item
// // @access Private
// exports.editItem = (req, res) => {
//   res.json({ type: 'POST api/auctions/item/edit' });
// };

// // @route POST api/auctions/item/delete
// // @desc add auction item
// // @access Private
// exports.deleteItem = (req, res) => {
//   res.json({ type: 'POST api/auctions/item/delete' });
// };

// @route POST api/auctions/item/bid
// @desc add bid to auction item
// @access Private
exports.bidItem = async (req, res) => {
  const { newBid } = req.body;
  const existingItem = await Item.findById(newBid.itemId);
  // eval(require('locus'));
  if (!existingItem) return res.json({ errors: { msg: 'Item does not exist' } });
  else if (moment(existingItem.endDate).format() < moment(Date.now()).format()) {
    return res.json({ errors: { msg: 'Auction is over for this Item' } });
  } else if (existingItem && existingItem.highestBid > newBid.bid) {
    return res.json({ errors: { msg: 'Your bid must be greater than current highest bid!' } });
  } else if (existingItem.minBid > newBid.bid) {
    return res.json({
      errors: { msg: 'Your bid must be greater than "minimum bid for this item"!' }
    });
  } else {
    let newItem = existingItem;
    newItem.highestBidder = req.body.user;
    newItem.highestBid = newBid.bid;
    const item = await Item.findByIdAndUpdate(newItem._id, newItem, {
      new: true, // returns updated item
      runValidators: true // runs setup validators to prevent inaccuracies
    }).exec(); // update item and return result
    res.json({
      item,
      success: {
        msg: `Successfully placed Bid!`
      }
    });
  }
};
