// import packages
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

// import models
const User = require('../../models/User');

// @route POST api/auth/register
// @desc Register user
// @access Public
exports.register = async (req, res) => {
  if (req.body.newUser.password !== req.body.passwordConfirm) {
    return res.status(400).json({ errors: { msg: 'Passwords do not match' } });
  }
  const findEmail = User.findOne({ email: req.body.newUser.email });
  const findUsername = User.findOne({ username: req.body.newUser.username });
  const [existingEmail, existingUsername] = await Promise.all([findEmail, findUsername]);
  console.log(existingEmail, existingUsername);
  if (existingEmail) return res.status(400).json({ errors: { msg: 'Email already exists' } });
  if (existingUsername) return res.status(400).json({ errors: { msg: 'Username already exists' } });
  const newUser = new User(req.body.newUser);
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(newUser.password, salt);
  newUser.password = hash;

  // set hardcoded JWT token for user
  const payload = { username: newUser.username, email: newUser.email, date: Date.now() }; // to generate JWT
  newUser.token = await jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '1y' });

  const user = await newUser.save();
  res.json(user);
};

// @route GET api/auth/test
// @desc Verify JWT token for hardcoded user
// @access Public
exports.authTest = async (req, res) => {
  const token = req.headers.authorization && req.headers.authorization.split(' ')[1];
  const user = await User.findOne({ token });
  if (!user) return res.status(403).json({ errors: { msg: 'User is not authorized' } });
  req.body.user = user;
  res.json({ user: 'User is authorized' });
};

// @middleware authorize user can bid
// @desc Verify hardcoded JWT token for hardcoded user
// @access Public
exports.checkUserAuth = async (req, res, next) => {
  const token = req.headers.authorization && req.headers.authorization.split(' ')[1];
  const user = await User.findOne({ token });
  if (!user) return res.status(403).json({ errors: { msg: 'User is not authorized' } });
  req.body.user = user;
  return next();
};

// eval(require('locus'))
