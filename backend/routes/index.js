// import packages
const express = require('express');
const router = express.Router();

// import route handlers
const auctions = require('./api/auctions');
const auth = require('./api/auth');

const { catchErrors } = require('../js/errorHandlers');

// @route GET api/test
// @desc Tests auctions route
// @access Public
router.get('/test', (req, res) => res.json({ msg: 'Test route works!' }));

router.get('/auctions', catchErrors(auctions.showItems));
router.post('/auctions/new', catchErrors(auth.checkUserAuth), catchErrors(auctions.addItem));
// router.get('/auctions/item', catchErrors(auctions.showItem));
// router.post('/auctions/item/edit', catchErrors(auth.checkUserAuth), catchErrors(auctions.editItem));
// router.post(
//   '/auctions/item/delete',
//   catchErrors(auth.checkUserAuth),
//   catchErrors(auctions.deleteItem)
// );
router.post('/auctions/item/bid', catchErrors(auth.checkUserAuth), catchErrors(auctions.bidItem));

router.get('/auth/test', catchErrors(auth.authTest));
router.post('/auth/register', catchErrors(auth.register));

module.exports = router;
