const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const mongodbErrorHandler = require('mongoose-mongodb-errors');

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    trim: true,
    required: 'You must provide a username'
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    required: 'Please provide an email address'
  },
  password: {
    type: String,
    required: 'Please provide a password'
  },
  token: {
    type: String,
    required: 'JWT token must be generated for user'
  },
  created: {
    type: Date,
    default: Date.now
  }
});

userSchema.plugin(mongodbErrorHandler); // more decriptive errors than standard MongoDB

module.exports = mongoose.model('User', userSchema);
