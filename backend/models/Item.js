const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const mongodbErrorHandler = require('mongoose-mongodb-errors');

const itemSchema = new mongoose.Schema({
  owner: {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    username: String
  },
  title: {
    type: String,
    trim: true,
    required: 'You must provide an item name'
  },
  desc: {
    type: String,
    trim: true,
    required: 'You must provide an item description'
  },
  image: {
    type: String,
    lowercase: true,
    trim: true
  },
  minBid: {
    type: Number,
    default: 1,
    min: 1
  },
  highestBid: {
    type: Number,
    min: 1
  },
  highestBidder: {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    username: String
  },
  created: {
    type: Date,
    default: Date.now
  },
  endDate: {
    type: Date,
    required: 'Please provide an auction end date',
    min: Date.now * 30000 // 30 second minimum
  }
});

itemSchema.plugin(mongodbErrorHandler); // more decriptive errors than standard MongoDB

module.exports = mongoose.model('Item', itemSchema);
