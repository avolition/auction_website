'use strict';
// ================================================
// SETUP
// ================================================
// import environmental variables from variables.env file
require('dotenv').config({ path: 'variables.env' });

// import packages
const express = require('express');
const helmet = require('helmet'); // secures by setting various HTTP headers
const bodyParser = require('body-parser');
const morgan = require('morgan'); // logger framework
const cors = require('cors');
const expressValidator = require('express-validator');
const mongoose = require('mongoose');

// import routes
const routes = require('./routes/index');

// import helper files
const helpers = require('./js/helpers');
const errorHandlers = require('./js/errorHandlers');

// create Express app
const app = express();

// ================================================
// APP CONFIG
// ================================================
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(helmet.referrerPolicy({ policy: 'no-referrer' }));
app.use(
  helmet.contentSecurityPolicy({ directives: { defaultSrc: ["'self'"], styleSrc: ["'self'"] } })
);
app.use(morgan('combined'));
app.use(cors()); // allow cross origin requests (change to whitelist domain / IP for production)
app.use(expressValidator()); // attaches validation methods to req

// middleware / vars to be accessed in all req's / res's
app.use((err, req, res, next) => {
  // if err parsing JSON, return error JSON object with status code 400
  if (err) {
    const errors = err.errors;
    errors.unshift({ msg: 'Could not decode request: JSON parsing failed' });
    console.log(errors);
    return res.status(400).send(errors);
  } else {
    res.locals.h = helpers;
    next();
  }
});

// app routes
app.use('/api', routes);

// If only validation errors, capture as flashError
app.use(errorHandlers.flashValidationErrors);

// If really bad error
if (app.get('env') === 'development') {
  app.use(errorHandlers.developmentErrors);
}

// production error handler
app.use(errorHandlers.productionErrors);

// export app to start script
module.exports = app;
